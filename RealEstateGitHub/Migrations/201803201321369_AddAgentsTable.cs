namespace RealEstateGitHub.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAgentsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AgentModel",
                c => new
                    {
                        AgentID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        PhoneNumber = c.String(nullable: false),
                        Email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AgentID);
            
            AddColumn("dbo.PropertyModel", "AgentModelID", c => c.Int(nullable: false));
            AddColumn("dbo.PropertyModel", "Agent_AgentID", c => c.Int());
            CreateIndex("dbo.PropertyModel", "Agent_AgentID");
            AddForeignKey("dbo.PropertyModel", "Agent_AgentID", "dbo.AgentModel", "AgentID");

            Sql("INSERT INTO AgentModel VALUES ('Albert', '78123456', 'Albert@RealEstateSample.com')");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PropertyModel", "Agent_AgentID", "dbo.AgentModel");
            DropIndex("dbo.PropertyModel", new[] { "Agent_AgentID" });
            DropColumn("dbo.PropertyModel", "Agent_AgentID");
            DropColumn("dbo.PropertyModel", "AgentModelID");
            DropTable("dbo.AgentModel");
        }
    }
}
