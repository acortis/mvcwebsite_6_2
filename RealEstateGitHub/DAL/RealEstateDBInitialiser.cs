﻿using RealEstateGitHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstateGitHub.DAL
{
    /// <summary>
    /// Used to set up initial data in the database.
    /// WARNING!!!
    /// Dropping and Creating databases can break database setup on live databases!
    /// </summary>
    /// <remarks>
    /// Consider inheriting DropCreateDatabaseIfModelChanges<RealEstateDBContext> instead
    /// </remarks>
    public class RealEstateDBInitialiser : System.Data.Entity.DropCreateDatabaseAlways<RealEstateDBContext>
    {
        protected override void Seed(RealEstateDBContext context)
        {
            var propertyList = new List<PropertyModel>
            {
                new PropertyModel() { Locality="Paola", PropertyType="Maisonette", ContractType="Sale", Bedrooms=2, FloorArea=110, Price=180000, ImageURL="listing.jpg" },
                new PropertyModel() { Locality="Hamrun", PropertyType="Apartment", ContractType="Sale", Bedrooms=3, FloorArea=105, Price=170000, ImageURL="listing.jpg" },
                new PropertyModel() { Locality="Marsa", PropertyType="Apartment", ContractType="Let", Bedrooms=2, FloorArea=90, Price=650, ImageURL="listing.jpg" },
                new PropertyModel() { Locality="Paola", PropertyType="Maisonette", ContractType="Sale", Bedrooms=2, FloorArea=110, Price=180000, ImageURL="listing.jpg" },
                new PropertyModel() { Locality="Paola", PropertyType="Maisonette", ContractType="Sale", Bedrooms=2, FloorArea=110, Price=180000, ImageURL="listing.jpg" }
            };

            propertyList.ForEach(p => context.Property.Add(p));
            context.SaveChanges();

            base.Seed(context);
        }
    }
}