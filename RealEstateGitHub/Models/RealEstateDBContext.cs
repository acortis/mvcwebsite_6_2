﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace RealEstateGitHub.Models
{
    /// <summary>
    /// The DbContext will represent a Database context
    /// that can be used by the website.
    /// </summary>
    public class RealEstateDBContext : DbContext
    {
        public RealEstateDBContext() : base("RealEstateDBContext")
        {
        }

        /// <summary>
        /// The DbContext will have a table of PropertyModel.
        /// </summary>
        public DbSet<PropertyModel> Property { get; set; }
        public DbSet<AgentModel> Agent { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}