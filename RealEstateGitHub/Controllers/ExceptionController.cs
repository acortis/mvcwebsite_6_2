﻿using RealEstateGitHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstateGitHub.Controllers
{
    public class ExceptionController : Controller
    {
         // GET: Exception
        public ActionResult ViewExceptionList()
        {
            IList<ExceptionDetail> exceptionList;

            HttpContext.Application.Lock();     // Application Context is locked, and only 1 thread can access this code
            
            try
            {
                if (HttpContext.Application["ExceptionList"] != null)
                {
                    exceptionList = (IList<ExceptionDetail>) HttpContext.Application["ExceptionList"];
                }
                else
                {
                    exceptionList = new List<ExceptionDetail>();
                }
            }
            finally
            {
                HttpContext.Application.UnLock();
            }
                
            return View(exceptionList);
        }
    }
}