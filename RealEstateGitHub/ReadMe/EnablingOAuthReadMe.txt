﻿The ReadMe is based on:
https://docs.microsoft.com/en-us/aspnet/core/security/authentication/social/google-logins?tabs=aspnetcore2x
and
https://docs.microsoft.com/en-us/aspnet/mvc/overview/security/create-an-aspnet-mvc-5-app-with-facebook-and-google-oauth2-and-openid-sign-on

Other usedful links include:
Facebook authentication:
https://docs.microsoft.com/en-us/aspnet/core/security/authentication/social/facebook-logins?tabs=aspnetcore2x

Mircosoft authentication:
https://docs.microsoft.com/en-us/aspnet/core/security/authentication/social/microsoft-logins?tabs=aspnetcore2x


Step 1:
Enable SSL in your website (see separate ReadMe) and make sure that OWIN is installed using NuGet.

Step 2:
Go to https://console.developers.google.com/ and create a project.

Step 3:
Enable Google+ API

Step 4:
Under credentials, create credentials: OAuth client id.

Step 5:
Set up the product name and select Web Application.
Enter the required information.

Note: If you have a valid url, make sure to set up the redirect and callback urls! 
This helps protect against XSS attacks.

Step 6:
The authorised redirect URL should be:
https://localhost:44377/signin-google

Replace 44377 with the SSL port used by your application.

Step 7:
Obtain the client ID and the app secret.

Step 8:
Update Startup.Auth.cs and enter the new credentials.
